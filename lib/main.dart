import 'package:flutter/material.dart';
import './image_input.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Camera Filter!',
      theme: ThemeData(
          primarySwatch: Colors.pink,
          accentColor: Colors.teal,
          buttonColor: Colors.teal),
      home: Scaffold(
        appBar: AppBar(
          title: Text('Camera filter!'),
        ),
        body: ImageInput(),
      ),
    );
    // return Scaffold(
    //   appBar: AppBar(title: Text('Camera filter!'),),
    //   body: Text('data'),
    // );
  }
}
