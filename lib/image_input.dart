import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageInput extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ImageInputState();
  }
}

class _ImageInputState extends State<ImageInput> {
  File _imageFile;
  String fileName;
  var greyScale = {
    'a': 255,
    'r': 255,
    'g': 255,
    'b': 255,
    'blendMode': BlendMode.saturation
  };
  var deuteranomaly = {
    'a': 180,
    'r': 50,
    'g': 30,
    'b': 0,
    'blendMode': BlendMode.plus
  };
  var original = {'a': 0, 'r': 0, 'g': 0, 'b': 0, 'blendMode': BlendMode.dst};
  var activeFilter;
  var _currentIndex;
  @override
  void initState() {
    super.initState();
    activeFilter = original;
    _currentIndex = 0;
  }

  void _getImage(BuildContext context, ImageSource source) {
    ImagePicker.pickImage(source: source).then((File image) {
      setState(() {
        _imageFile = image;
      });
      Navigator.pop(context);
    });
  }

  void _deleteImage() {
    setState(() {
      _imageFile = null;
    });
    Navigator.pop(context);
  }

  void _openImagePicker(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: _imageFile == null ? 158 : 200,
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Text(
                  'Pick an image',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                FlatButton(
                  child: Text('Use camera!'),
                  onPressed: () {
                    _getImage(context, ImageSource.camera);
                  },
                ),
                FlatButton(
                  child: Text(
                    'Choose from gallery!',
                  ),
                  onPressed: () {
                    _getImage(context, ImageSource.gallery);
                  },
                ),
                _imageFile == null
                    ? Text('')
                    : FlatButton(
                        child: Text('Delete!'),
                        onPressed: () {
                          _deleteImage();
                        },
                      )
              ],
            ),
          );
        });
  }

  Widget _buildImage() {
    return Image.file(
      _imageFile,
      fit: BoxFit.cover,
      height: 400.0,
      alignment: Alignment.topCenter,
      width: MediaQuery.of(context).size.width,
      color: Color.fromARGB(activeFilter['a'], activeFilter['r'],
          activeFilter['g'], activeFilter['b']),
      filterQuality: FilterQuality.high,
      colorBlendMode: activeFilter['blendMode'],
    );
  }

  Widget _buildFilterList() {
    return BottomNavigationBar(
      onTap: (index) {
        setState(() {
          if (index == 0) {
            activeFilter = original;
          }
          if (index == 1) {
            activeFilter = greyScale;
          }
          if (index == 2) {
            activeFilter = deuteranomaly;
          }
          _currentIndex = index;
        });
      },
      currentIndex: _currentIndex == null ? 0 : _currentIndex,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          title: Text('Original'),
          icon: Icon(Icons.filter_none),
        ),
        BottomNavigationBarItem(
          title: Text('Greyscale'),
          icon: Icon(Icons.filter),
        ),
        BottomNavigationBarItem(
          title: Text('Top Secret Filter'),
          icon: Icon(Icons.photo_filter),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(height: 16.0),
        Center(
          child: FloatingActionButton(
            onPressed: () {
              _openImagePicker(context);
            },
            child: Icon(Icons.camera),
          ),
        ),
        SizedBox(
          height: 10.0,
        ),
        _imageFile == null
            ? Text('Please choose an image or take a picture')
            : Column(children: <Widget>[_buildFilterList(), _buildImage()])
      ],
    );
  }
}
